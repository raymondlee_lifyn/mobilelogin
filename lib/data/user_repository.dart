import 'package:mobilelogin/data/models/user.dart';
import 'package:mobilelogin/data/services/api/auth_api.dart';

class UserRepository {
  List<String> countriesCode = ['+60', '+65'];

  // Select Countries for Personal Info
  List<String> countries = ['Malaysia', 'Singapore'];

  final AuthApi _authApi = AuthApi();

  Future<User> getUser(String phoneNumber) async {
    try {
      final user = await _authApi.phoneLogin(phoneNumber);

      // return User(
      //   userId: user['id'],
      //   email: user['email'],
      //   name: user['name'],
      //   phoneNumber: user['phone_number'],
      //   profilePictureUrl: user['profile_picture_url'],
      // );

      return User.fromJSON(user);
    } catch (err) {
      throw 'Unable to get user';
    }
  }

  List<String> getCountryCodes() {
    return this.countriesCode;
  }

  // Select Countries for Personal Info
  List<String> getCountries() {
    return this.countries;
  }

  String validatePhone(phoneNumber) {
    if (phoneNumber.length > 7) {
      return null;
    }

    return 'Phone must be up to 8 number';
  }

  String validateName(String value) {
    if (value.length > 7) {
      return null;
    }
    return "Name must be 5 characters and above.";
  }

  String validateEmail(String value) {
    if (value.isEmpty) {
      // The form is empty
      return "Enter email address";
    }
    // This is just a regular expression for email addresses
    String p = "[a-zA-Z0-9\+\.\_\%\-\+]{1,256}" +
        "\\@" +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
        "(" +
        "\\." +
        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
        ")+";
    RegExp regExp = new RegExp(p);

    if (regExp.hasMatch(value)) {
      // So, the email is valid
      return null;
    }

    // The pattern of the email didn't match the regex above.
    return 'Email is not valid';
  }
}
