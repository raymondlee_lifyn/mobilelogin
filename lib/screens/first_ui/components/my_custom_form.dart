import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/data/user_repository.dart';

class MyCustomForm extends StatefulWidget {
  final GlobalKey<FormState> formKey;
  final LoginBloc loginBloc;

  const MyCustomForm({this.loginBloc, this.formKey});

  @override
  _MyCustomFormState createState() => _MyCustomFormState();
}

class _MyCustomFormState extends State<MyCustomForm> {
  final controller = TextEditingController();
  final _countryCodeController = TextEditingController(text: '+60');
  bool autoValid = false;

  final UserRepository _userRepository = UserRepository();

  LoginBloc get _loginBloc => widget.loginBloc;

  @override
  void initState() {
    super.initState();

    controller.addListener(() {
      return controller.text;
    });
  }

  void validate() {
    if (widget.formKey.currentState.validate()) {
      _loginBloc
          .setPhoneNumber('${_countryCodeController.text}${controller.text}');
      _loginBloc.add(GetUser(_countryCodeController.text, controller.text));
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _FormHeader(size),
        SizedBox(
          height: size.height / 30,
        ),
        _FormLabel(size),
        SizedBox(
          height: size.height / 40,
        ),
        Container(
          height: size.width / 5,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Start CountryCode DropDownButton
              Container(
                width: size.width * 0.24,
                height: size.height / 13.5,
                padding: EdgeInsets.symmetric(
                  horizontal: size.width / 20,
                  vertical: 0,
                ),
                decoration: BoxDecoration(
                  color: Color(0xFF019D9A),
                  borderRadius: BorderRadius.circular(36),
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButtonFormField(
                    value: _countryCodeController.text,
                    iconSize: size.width / 15,
                    items: _userRepository.getCountryCodes().map((value) {
                      return DropdownMenuItem(
                        value: value,
                        child: Text(
                          value,
                          style: TextStyle(
                            fontSize: size.width * 0.04,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (String value) {
                      setState(() {
                        _countryCodeController.text = value;
                      });
                    },
                  ),
                ),
              ),
              // End CountryCode DropDownButton
              SizedBox(
                width: size.width * 0.03,
              ),
              // Start Phone No. TextFormField
              FormField<String>(
                validator: (String value) {
                  return _userRepository.validatePhone(controller.text);
                },
                autovalidate: autoValid,
                builder: (FormFieldState<String> state) {
                  return Column(
                    children: <Widget>[
                      Container(
                        width: size.width * 0.55,
                        child: TextFormField(
                          controller: controller,
                          keyboardType: TextInputType.phone,
                          style: TextStyle(
                            fontSize: size.width * 0.04,
                          ),
                          decoration: InputDecoration(
                            hintText: 'Enter phone no.',
                            hintStyle: TextStyle(
                              fontFamily: 'Ubuntu',
                            ),
                            fillColor: Color(0xFF019D9A),
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: size.width * 0.02,
                                horizontal: size.width / 20),
                            errorStyle: TextStyle(fontSize: 0, height: 0),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(36),
                              borderSide: BorderSide.none,
                            ),
                          ),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                      (state.hasError)
                          ? Container(
                              padding: EdgeInsets.only(top: size.width / 100),
                              child: Text(
                                state.errorText,
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: size.width * 0.035),
                              ),
                            )
                          : Container()
                    ],
                  );
                },
              ),
              // End Phone No. TextFormField
            ],
          ),
        ),
        SizedBox(
          height: size.height / 50,
        ),
        // Start Next Button
        Container(
          width: size.width * 0.88,
          height: size.height * 0.08,
          margin: EdgeInsets.only(top: size.width * 0.08),
          child: RaisedButton(
            onPressed: validate,
            child: Text(
              'Next',
              style: TextStyle(
                color: Color(0xFF01B9B4),
                fontFamily: 'Ubuntu',
                fontSize: 17,
                fontWeight: FontWeight.bold,
              ),
            ),
            color: Color(0xFFFFFFFF),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40),
            ),
          ),
        ),
        // End Next Button
      ],
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}

class _FormHeader extends StatelessWidget {
  final Size size;

  const _FormHeader(this.size);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        'Log in to your account',
        style: TextStyle(
          color: Colors.white,
          fontFamily: 'Ubuntu',
          fontSize: 25,
        ),
      ),
    );
  }
}

class _FormLabel extends StatelessWidget {
  final Size size;

  const _FormLabel(this.size);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Text(
            'Country Code',
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Ubuntu',
            ),
          ),
          SizedBox(
            width: size.width * 0.07,
          ),
          Text(
            'Phone No.',
            style: TextStyle(
              color: Colors.white,
              fontFamily: 'Ubuntu',
            ),
          ),
        ],
      ),
    );
  }
}
