import 'package:flutter/material.dart';

class TitleLayout extends StatelessWidget {
  const TitleLayout({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.5,
      height: MediaQuery.of(context).size.height * 0.5,
      alignment: Alignment.center,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: 150,
          maxHeight: 90,
        ),
        child: RichText(
          text: TextSpan(
            text: 'lifyn',
            style: TextStyle(
              fontFamily: 'Ubuntu',
              fontWeight: FontWeight.bold,
              fontSize: 60,
              letterSpacing: 5,
            ),
            children: <TextSpan>[
              TextSpan(
                text: 'Living Together',
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                  fontSize: 10,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
