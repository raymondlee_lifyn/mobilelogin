import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/constants.dart';
import 'package:mobilelogin/screens/phone_verification/components/pin_verification_field.dart';
import 'package:mobilelogin/screens/personal_info/personal_info.dart';

class PhoneVerification extends StatefulWidget {
  PhoneVerification();

  @override
  _PhoneVerificationState createState() => _PhoneVerificationState();
}

class _PhoneVerificationState extends State<PhoneVerification> {
  final _smsCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final Size _size = MediaQuery.of(context).size;
    final LoginBloc _loginBloc = context.bloc<LoginBloc>();

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Phone Verification',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () => Navigator.popAndPushNamed(context, '/'),
        ),
      ),
      backgroundColor: Colors.white,
      body: Container(
        margin: EdgeInsets.symmetric(
          horizontal: _size.width * 0.07,
          vertical: _size.height * 0.03,
        ),
        child: BlocConsumer<LoginBloc, LoginState>(
          listener: (context, state) {
            if (state is! VerifyPhoneError) {
              if (state is UpdateUserInfo) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (_) => BlocProvider.value(
                      value: _loginBloc,
                      child: PersonalInfo(),
                    ),
                  ),
                );
              }
            }

            if (state is VerifyPhoneError) {
              return Scaffold.of(context).showSnackBar(
                SnackBar(content: Text('${state.error}')),
              );
            }
          },
          builder: (context, state) {
            if (state is SMSSent || state is VerifyPhoneError) {
              if (state is VerifyPhoneError) {
                print(state.error);
              }

              // if (state is VerifyPhoneInProgress) {
              //   print('loading');
              //   return kLoadingWidget();
              // }

              return Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Please enter verification code that has been to sent to your phone no.',
                  ),
                  SizedBox(
                    height: _size.height / 20,
                  ),
                  Text(
                    'Verification Code',
                    style: TextStyle(color: kPrimaryColor),
                  ),
                  Expanded(
                    flex: 1,
                    child: PinVerificationField(
                      smsCodeController: _smsCodeController,
                    ),
                  ),
                  RaisedButton(
                    onPressed: () {
                      _loginBloc
                          .add(VerifyPhone(smsCode: _smsCodeController.text));
                    },
                    child: Container(
                      child: Text(
                        'Continue',
                        style: TextStyle(
                          fontSize: _size.width * 0.05,
                          color: Colors.white,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: _size.width * 0.28,
                        vertical: _size.height * 0.013,
                      ),
                      decoration: BoxDecoration(
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: kPrimaryColor,
                            blurRadius: 13,
                            offset: Offset(0, 2),
                          ),
                        ],
                      ),
                    ),
                    color: kPrimaryColor,
                    elevation: 2,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(36),
                    ),
                  ),
                  SizedBox(
                    height: _size.height / 100,
                  ),
                  FlatButton(
                    color: Colors.grey[350],
                    onPressed: null,
                    child: Container(
                      child: Text(
                        'Resend Code (30s)',
                        style: TextStyle(
                          fontSize: _size.width * 0.04,
                        ),
                      ),
                      padding: EdgeInsets.symmetric(
                        horizontal: _size.width * 0.21,
                        vertical: _size.height * 0.013,
                      ),
                    ),
                    disabledColor: Colors.grey[200],
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(36),
                    ),
                  ),
                ],
              );
            }

            // Prevent build function return null
            return Container();
          },
        ),
      ),
    );
  }
}
