import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/screens/first_ui/firstUI.dart';
import 'package:slide_to_confirm/slide_to_confirm.dart';

class SecondUI extends StatefulWidget {
  const SecondUI({Key key}) : super(key: key);

  @override
  _SecondUIState createState() => _SecondUIState();
}

class _SecondUIState extends State<SecondUI> {
  final String fontFamily = 'Ubuntu';
  final _smsCodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final _loginBloc = context.bloc<LoginBloc>();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: size.width * 0.03, vertical: size.height * 0.06),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              profileWidget(size),
              Divider(
                color: Colors.white,
                height: size.width / 8,
              ),
              BlocConsumer<LoginBloc, LoginState>(
                listener: (context, state) {
                  // return ButtonTheme(
                  //   minWidth: size.width * 0.08,
                  //   height: size.height * 0.08,
                  //   child: RaisedButton(
                  //     onPressed: state is! LoginSuccess
                  //         ? () {
                  //             return Navigator.of(context).pop(FirstUI());
                  //           }
                  //         : null,
                  //     child: Text(
                  //       'Switch Account',
                  //       style: TextStyle(
                  //         fontSize: size.width * 0.05,
                  //         color: Colors.white,
                  //         fontFamily: fontFamily,
                  //       ),
                  //     ),
                  //     color: Color(0xFF01B9B4),
                  //     shape: RoundedRectangleBorder(
                  //       borderRadius: BorderRadius.circular(20),
                  //     ),
                  //   ),
                  // );
                },
                builder: (context, state) {
                  if (state is LoginSuccess) {
                    return Text(
                      'Welcome home, ${state.name}.',
                      style: TextStyle(
                        color: Color(0xFF424249),
                        fontWeight: FontWeight.bold,
                        fontSize: size.width * 0.07,
                        fontFamily: fontFamily,
                      ),
                    );
                  }
                  return Container();
                },
              ),
              SizedBox(height: size.height * 0.03),
              // Start Switch Account Button
              ButtonTheme(
                minWidth: size.width * 0.08,
                height: size.height * 0.08,
                child: RaisedButton(
                  onPressed: () {
                    return Navigator.of(context).pop(FirstUI());
                  },
                  child: Text(
                    'Switch Account',
                    style: TextStyle(
                      fontSize: size.width * 0.05,
                      color: Colors.white,
                      fontFamily: fontFamily,
                    ),
                  ),
                  color: Color(0xFF01B9B4),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
              ),
              // End Switch Account Button
              SizedBox(height: size.height / 100),
              Text(
                'Not you?',
                style: TextStyle(
                  fontFamily: fontFamily,
                ),
              ),
              SizedBox(height: size.height * 0.15),
              // Start Slide to Continue
              ConfirmationSlider(
                onConfirmation: () {
                  _loginBloc.add(VerifyPhone(smsCode: _smsCodeController.text));
                },
                height: 55,
                width: 350,
                backgroundColor: Colors.blueGrey[50],
                foregroundColor: Colors.white,
                iconColor: Color(0xFF01B9B4),
                text: 'Slide to Continue',
                textStyle: TextStyle(
                  fontSize: size.width * 0.045,
                  color: Colors.grey,
                  fontFamily: fontFamily,
                ),
              ),
              // End Slide to Continue
            ],
          ),
        ),
      ),
    );
  }
}

// Profile Picture
Widget profileWidget(Size size) {
  return Container(
    child: CircleAvatar(
      backgroundImage: AssetImage('assets/profilepic.jpg'),
      radius: size.width * 0.28,
    ),
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      border: Border.all(
        color: Colors.white,
        width: size.width * 0.03,
      ),
    ),
  );
}
