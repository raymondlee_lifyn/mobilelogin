import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/constants.dart';
import 'package:mobilelogin/data/user_repository.dart';
import 'package:mobilelogin/screens/my_flutter_app_icons.dart';

class MyPersonalForm extends StatefulWidget {
  final LoginBloc loginBloc;

  const MyPersonalForm({this.loginBloc});

  @override
  _MyPersonalFormState createState() => _MyPersonalFormState();
}

class _MyPersonalFormState extends State<MyPersonalForm> {
  final formKey = GlobalKey<FormState>();
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final countryController = TextEditingController(text: 'Malaysia');
  bool autoValid = false;

  final UserRepository _userRepository = UserRepository();

  LoginBloc get _loginBloc => widget.loginBloc;

  void updateUser() {
    // TODO: submit to backend for updating user details
    // print('name: ' + nameController.text);
    // print('email: ' + emailController.text);
    // print('country: ' + countryController.text);
    // print(_loginBloc.toString());

    if (formKey.currentState.validate()) {
      print('success');

      _loginBloc.add(UpdateUserDetails(
        name: nameController.text,
        email: emailController.text,
        country: countryController.text,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    // LoginBloc loginBloc = BlocProvider.of<LoginBloc>(context);
    Size size = MediaQuery.of(context).size;

    return BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        if (state is UpdateUserInProgress) {
          return kLoadingWidget();
        }

        print(state.toString());

        // return this.initialForm(context, _loginBloc);
        return Form(
          key: formKey,
          autovalidate: autoValid,
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: size.width * 0.07, vertical: size.height * 0.03),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Please fill up your personal information.'),

                SizedBox(
                  height: size.height / 50,
                ),
                // Start NAME TextField
                Container(
                  child: Text(
                    'Name',
                    style: TextStyle(
                      fontSize: size.width * 0.04,
                      color: kPrimaryColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: size.height / 50,
                ),
                Container(
                  height: size.height / 9,
                  child: TextFormField(
                    controller: nameController,
                    validator: _userRepository.validateName,
                    style: TextStyle(
                      fontSize: size.width * 0.04,
                    ),
                    decoration: InputDecoration(
                      hintText: 'Enter your Name',
                      hintStyle: TextStyle(fontFamily: 'Ubuntu'),
                      filled: true,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: size.width * 0.02,
                          horizontal: size.width / 20),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(36),
                      ),
                    ),
                  ),
                ),
                // End NAME TextField
                // Start EMAIL TextField
                Text(
                  'Email',
                  style: TextStyle(
                    fontSize: size.width * 0.04,
                    color: kPrimaryColor,
                  ),
                ),

                SizedBox(
                  height: size.height / 50,
                ),
                Container(
                  height: size.height / 9,
                  child: TextFormField(
                    controller: emailController,
                    validator: _userRepository.validateEmail,
                    style: TextStyle(
                      fontSize: size.width * 0.04,
                    ),
                    decoration: InputDecoration(
                      hintText: 'Enter your Email',
                      hintStyle: TextStyle(fontFamily: 'Ubuntu'),
                      filled: true,
                      contentPadding: EdgeInsets.symmetric(
                          vertical: size.width * 0.02,
                          horizontal: size.width / 20),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(36),
                      ),
                    ),
                  ),
                ),
                // End EMAiL TextField
                // Start COUNTRY DropDownButton
                Text(
                  'Country',
                  style: TextStyle(
                    fontSize: size.width * 0.04,
                    color: kPrimaryColor,
                  ),
                ),
                SizedBox(
                  height: size.height / 50,
                ),
                Container(
                  height: size.height / 9,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(36),
                  ),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButtonFormField(
                      value: countryController.text,
                      iconSize: size.width / 15,
                      items: _userRepository.getCountries().map((value) {
                        return DropdownMenuItem(
                          value: value,
                          child: Text(
                            value,
                            style: TextStyle(
                              fontSize: size.width * 0.04,
                            ),
                          ),
                        );
                      }).toList(),
                      onChanged: (String value) {
                        setState(() {
                          countryController.text = value;
                        });
                      },
                      elevation: 1,
                      decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.symmetric(
                            vertical: size.width * 0.02,
                            horizontal: size.width / 20),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(36),
                        ),
                      ),
                    ),
                  ),
                ),
                // End COUNTRY DropDownButton
                SizedBox(
                  height: size.height / 50,
                ),
                // Start SUBMIT Button
                RaisedButton(
                  onPressed: updateUser,
                  child: Container(
                    child: Text(
                      'Submit',
                      style: TextStyle(
                        fontSize: size.width * 0.05,
                        color: Colors.white,
                      ),
                    ),
                    padding: EdgeInsets.symmetric(
                      horizontal: size.width * 0.31,
                      vertical: size.height * 0.02,
                    ),
                    decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: kPrimaryColor,
                          blurRadius: 13,
                          offset: Offset(0, 2),
                        ),
                      ],
                    ),
                  ),
                  color: kPrimaryColor,
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(36),
                  ),
                ),
                // End SUBMIT Button
                Divider(
                  color: Colors.black,
                  height: size.width / 6,
                  thickness: 1,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton.icon(
                      color: Colors.white,
                      icon: Icon(
                        MyFlutterApp.facebook,
                        size: size.height * 0.04,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        // TODO: continue with FACEBOOK login
                        _loginBloc.add(UpdateUserWithFacebook());
                      },
                      label: Container(
                        width: size.width * 0.25,
                        child: Text(
                          'Continue with Facebook',
                          style: TextStyle(
                            fontFamily: 'MyFlutterApp',
                          ),
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(36),
                      ),
                    ),
                    RaisedButton.icon(
                      color: Colors.white,
                      icon: Icon(
                        MyFlutterApp.google,
                        size: size.height * 0.04,
                        color: Colors.red[600],
                      ),
                      onPressed: () {
                        // TODO: continue with GOOGLE login
                        _loginBloc.add(UpdateUserWithGoogle());
                      },
                      label: Container(
                        width: size.width * 0.25,
                        child: Text(
                          'Continue with Google',
                          style: TextStyle(
                            fontFamily: 'MyFlutterApp',
                          ),
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(36),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  // Widget initialForm(BuildContext context, LoginBloc loginBloc) {
  //   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  //   Size size = MediaQuery.of(context).size;

  // }
}
