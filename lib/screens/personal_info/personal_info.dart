import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mobilelogin/bloc/login/login_bloc.dart';
import 'package:mobilelogin/screens/personal_info/components/personal_form.dart';

class PersonalInfo extends StatefulWidget {
  @override
  _PersonalInfoState createState() => _PersonalInfoState();
}

class _PersonalInfoState extends State<PersonalInfo> {
  LoginBloc _loginBloc;

  @override
  Widget build(BuildContext context) {
    // _loginBloc = context.bloc<LoginBloc>();
    _loginBloc = BlocProvider.of<LoginBloc>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Personal Info.',
          style: TextStyle(
            color: Colors.black,
            fontFamily: 'Ubuntu',
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 1,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          color: Colors.black,
          onPressed: () => Navigator.popAndPushNamed(context, '/'),
        ),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: MyPersonalForm(
          loginBloc: _loginBloc,
        ),
      ),
    );
  }

  // @override
  // void dispose() {
  //   _loginBloc.close();
  //   super.dispose();
  // }
}
