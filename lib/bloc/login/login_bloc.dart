import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/services.dart';
import 'package:mobilelogin/data/services/api/auth_api.dart';

import 'package:mobilelogin/data/user_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  // Dependency Injection (DI)
  final UserRepository _userRepository;

  LoginBloc(this._userRepository) : super(LoginInitial());

  final AuthApi _authApi = AuthApi();

  String _phoneNumber;
  String _verificationId;
  int _forceResendToken;
  FirebaseAuth auth = FirebaseAuth.instance;

  setPhoneNumber(String value) {
    _phoneNumber = value;
  }

  Future<void> sendSMS(String phoneNumber) async {
    final successCodeSent = (String verificationId, [int forceResendToken]) {
      _verificationId = verificationId;
      _forceResendToken = forceResendToken;
    };

    final timeoutCodeRetrieval = (String verifiationId) {
      _verificationId = _verificationId;
      print('code timeout');
    };

    final verificationCompleted = (AuthCredential phoneAuthCredential) async {
      this.add(VerifyPhone(authCredential: phoneAuthCredential));

      // final AuthResult _user =
      //     await auth.signInWithCredential(phoneAuthCredential);

      // IdTokenResult uid = await _user.user.getIdToken();

      // await _authApi.sendIdToken(uid.token);

      print('verified success.');
    };

    await auth.verifyPhoneNumber(
      phoneNumber: phoneNumber,
      timeout: const Duration(seconds: 60),
      codeSent: successCodeSent,
      codeAutoRetrievalTimeout: timeoutCodeRetrieval,
      verificationCompleted: verificationCompleted,
      verificationFailed: (AuthException e) {
        if (e.code == 'invalid-phone-number') {
          print('The provided phone number is not valid.');
        }
      },
    );
  }

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is GetUser) {
      final _phoneNum = event.countryCode + event.phoneNumber;

      try {
        yield LoginLoading();
        final user = await _userRepository.getUser(_phoneNum);

        if (user != null) {
          yield LoginSuccess(user.name, user.profilePictureUrl);
        }

        await sendSMS(user.phoneNumber);
      } catch (err) {
        yield SMSSent();

        await sendSMS(_phoneNum);
      }
    }

    if (event is SwitchAccount) {
      yield LoginInitial();
    }

    if (event is VerifyPhone) {
      yield SMSSent();

      try {
        // yield VerifyPhoneInProgress();

        AuthCredential _authCredential;

        if (event.authCredential != null) {
          _authCredential = event.authCredential;
        } else {
          _authCredential = PhoneAuthProvider.getCredential(
            verificationId: _verificationId,
            smsCode: event.smsCode,
          );

          final AuthResult _user =
              await auth.signInWithCredential(_authCredential);

          IdTokenResult uid = await _user.user.getIdToken();

          await _authApi.sendIdToken(uid.token);

          // yield SMSSent();

          // await sendSMS(_phoneNumber);

          yield UpdateUserInfo();
        }
      } on PlatformException catch (err) {
        print(err.message);
        yield VerifyPhoneError('Invalid verification code');
      }
    }

    if (event is UpdateUserDetails) {
      try {
        yield UpdateUserInProgress();

        final response =
            await _authApi.updateUser(event.name, event.email, _phoneNumber);

        print(response);

        yield UpdateUserSuccess();
      } catch (err) {
        yield UpdateUserFailed(err.toString());
      }
    }

    if (event is UpdateUserWithGoogle) {
      try {
        yield UpdateUserInProgress();

        final response = await _authApi.signInWithGoogle();

        print('google response: $response');

        yield UpdateUserSuccess();
      } catch (err) {
        yield UpdateUserFailed(err.toString());
      }
    }

    if (event is UpdateUserWithFacebook) {
      try {
        yield UpdateUserInProgress();

        final response = await _authApi.signInWithFacebook();
        print('facebook response: $response');

        yield UpdateUserSuccess();
      } catch (err) {
        yield UpdateUserFailed(err.toString());
      }
    }
  }
}
