import 'package:flutter/material.dart';

const Color kPrimaryColor = Color(0xFF01B9B4);
const String fontFamily = 'Ubuntu';

Widget kLoadingWidget() {
  return Center(
    child: CircularProgressIndicator(),
  );
}
